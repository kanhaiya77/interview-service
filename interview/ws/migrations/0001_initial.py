# Generated by Django 2.2.2 on 2020-10-22 17:49

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Interview',
            fields=[
                ('creator_id', models.UUIDField(default=None, help_text='ID of the creator of Object', null=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True, help_text='Datetime of Creation of Object')),
                ('last_modifier_id', models.UUIDField(default=None, help_text='ID of the last user who has modified', null=True)),
                ('last_modified_date', models.DateTimeField(auto_now=True, help_text='Datetime of the last modification of Object')),
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, help_text='Interview Id of the interview object', primary_key=True, serialize=False)),
                ('title', models.CharField(help_text='title of interview', max_length=100)),
                ('description', models.CharField(help_text='description about interview', max_length=200, null=True)),
                ('role', models.CharField(help_text='role for interview', max_length=50)),
                ('start_time', models.DateTimeField(auto_now_add=True, help_text='The start time of the interview')),
                ('end_time', models.DateTimeField(default=None, help_text='The Completion time of the interview', null=True)),
                ('status', models.CharField(choices=[('SE', 'Scheduled'), ('AC', 'Active'), ('CT', 'Completed')], max_length=50)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Participant',
            fields=[
                ('creator_id', models.UUIDField(default=None, help_text='ID of the creator of Object', null=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True, help_text='Datetime of Creation of Object')),
                ('last_modifier_id', models.UUIDField(default=None, help_text='ID of the last user who has modified', null=True)),
                ('last_modified_date', models.DateTimeField(auto_now=True, help_text='Datetime of the last modification of Object')),
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, help_text='Participant Id of the participant object', primary_key=True, serialize=False)),
                ('role', models.CharField(help_text='role of the participant', max_length=50)),
                ('name', models.CharField(help_text='name of participant', max_length=50)),
                ('email', models.EmailField(max_length=254)),
                ('end_time', models.DateTimeField(default=None, help_text='The Completion time of the interview', null=True)),
                ('feedback', models.CharField(max_length=50, null=True)),
                ('interview_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ws.Interview')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='CodeSubmission',
            fields=[
                ('creator_id', models.UUIDField(default=None, help_text='ID of the creator of Object', null=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True, help_text='Datetime of Creation of Object')),
                ('last_modifier_id', models.UUIDField(default=None, help_text='ID of the last user who has modified', null=True)),
                ('last_modified_date', models.DateTimeField(auto_now=True, help_text='Datetime of the last modification of Object')),
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, help_text='Id of the code submission object', primary_key=True, serialize=False)),
                ('status', models.CharField(choices=[('QE', 'Queued'), ('EX', 'Executing'), ('CT', 'Completed')], max_length=50)),
                ('fire_time', models.DateTimeField(auto_now_add=True, help_text='when code execution was fired')),
                ('interview_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ws.Interview')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
