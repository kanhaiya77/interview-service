from rest_framework.viewsets import ViewSet
from rest_framework.response import Response
from django.core.exceptions import ValidationError

from cs_utils.authentication import JWTAuthentication
from cs_utils.parsers import JSONAuditParser
from cs_utils.decorators import user_has_roles
from cs_utils.doc_utils import schema_without_field, x_access_token_parameter, x_refresh_token_parameter
from cs_utils.paginator_mixin import PaginatorMixin

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

from ws.serializers import InterviewSerializer

from ws.models import Interview

interview_id_parameter = openapi.Parameter(
    'interview_id',
    openapi.IN_PATH,
    'The id of the interview',
    type=openapi.TYPE_STRING,
    required=True,
)

class InterviewViewSet(ViewSet,PaginatorMixin):
    authentication_classes = (JWTAuthentication, )

    @swagger_auto_schema(
    operation_description='Create a new Interview',
    operation_summary='Create Interview',
    responses={
        400: 'Data input is not according to constraints',
        201: 'ID of created Interview in form of ```{"id": <UUID>}```',
    },
    request_body=InterviewSerializer,
    tags=['Interview'],
    manual_parameters=[x_access_token_parameter, x_refresh_token_parameter],
    field_inspectors=[schema_without_field(['creator_id', 'last_modifier_id'])]
    )
    @user_has_roles(['super_admin'])
    def create_Interview(self, req):
        data = JSONAuditParser().parse(req)
        serialized_data = InterviewSerializer(data=data)
        if serialized_data.is_valid():
            interview_settings = serialized_data.save()
            return Response({'id': interview_settings.id}, status=201)
        else:
            return Response(serialized_data.errors, status=400)
    
    @swagger_auto_schema(
    operation_description='Get all the interviews',
    operation_summary='Get all Interviews',
    responses={
        400: 'Unable to fetch the interviews',
        200: InterviewSerializer(many=True)
    },
    tags=['Interview'],
    manual_parameters=[x_access_token_parameter, x_refresh_token_parameter],
    field_inspectors=[schema_without_field(['creator_id', 'last_modifier_id'])]
    )
    @user_has_roles(['super_admin'])
    def get_all_interviews(self, req):
       interviews = Interview.objects.all()
       interview_serializer = InterviewSerializer(interviews,many=True)
       return self.get_paginated_response(interview_serializer.data)

    @swagger_auto_schema(
        operation_description='Get Interview by ID',
        operation_summary='Get Interview Details',
        responses={
            200: InterviewSerializer,
            400: 'Invalid data or invalid UUID for Interview ID',
            404: 'Invalid Interview ID',
        },
        tags=['Interview'],
        manual_parameters=[x_access_token_parameter, x_refresh_token_parameter, interview_id_parameter],
        field_inspectors=[schema_without_field(['creator_id', 'last_modifier_id'])]
    )
    @user_has_roles(['super_admin'])
    def get_interview(self, req, interview_id):
        try:
            interview_entity = Interview.objects.get(pk=interview_id)
            serialized_interview = InterviewSerializer(interview_entity)
            return Response(serialized_interview.data, status=200)
        except Interview.DoesNotExist as e:
            return Response({'msg': 'Object with given ID does not exist'}, status=404)
        except ValidationError as e:
            return Response({'msg': str(e)}, status=400)

    @swagger_auto_schema(
        operation_description='Delete Interview by ID',
        operation_summary='Delete Interview Details',
        responses={
            204: 'Delete Success',
            400: 'Invalid data or invalid UUID for Interview ID',
            404: 'Invalid Interview ID',
        },
        tags=['Interview'],
        manual_parameters=[x_access_token_parameter, x_refresh_token_parameter, interview_id_parameter],
        field_inspectors=[schema_without_field(['creator_id', 'last_modifier_id'])]
    )
    @user_has_roles(['super_admin'])
    def delete_interview(self, req, interview_id):
        try:
            interview_entity = Interview.objects.get(pk=interview_id)
            interview_entity.delete()
            return Response({'msg': 'Object deletetion success'}, status=204)
        except Interview.DoesNotExist as e:
            return Response({'msg': 'Object with ID does not exist'}, status=404)
        except ValidationError as e:
            return Response({'msg': str(e)}, status=400)

    @swagger_auto_schema(
        operation_description='update Interview by ID',
        operation_summary='update Interview',
        request_body='InterviewSerializer',
        responses={
            200: 'update successful',
            400: 'Invalid data or invalid UUID for Interview ID',
            404: 'Invalid Interview ID',
        },
        tags=['Interview'],
        manual_parameters=[x_access_token_parameter, x_refresh_token_parameter, interview_id_parameter],
        field_inspectors=[schema_without_field(['creator_id', 'last_modifier_id'])]
    )
    @user_has_roles(['super_admin'])
    def update_interview(self, req, interview_id):
        try:
            interview_entity = Interview.objects.get(pk=interview_id)
            data = JSONAuditParser().parse(req)
            serialized_interview = InterviewSerializer(interview_entity, data=data, partial=True)
            if serialized_interview.is_valid():
                serialized_interview.save()
                return Response({'msg': 'Given Interview updated success'}, status=200)
            else:
                return Response(serialized_interview.errors, status=400)
        except Interview.DoesNotExist as e:
            return Response({'msg': 'Object with given ID does not exist'}, status=404)
        except ValidationError as e:
            return Response({'msg': str(e)}, status=400)

   
