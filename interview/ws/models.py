from django.db import models
import uuid


class AuditStamp(models.Model):
    creator_id = models.UUIDField(help_text='ID of the creator of Object', default=None, null=True)
    creation_date = models.DateTimeField(auto_now_add=True, help_text='Datetime of Creation of Object')
    last_modifier_id = models.UUIDField(help_text='ID of the last user who has modified', default=None, null=True)
    last_modified_date = models.DateTimeField(auto_now=True, help_text='Datetime of the last modification of Object')

    class Meta:
        abstract = True


class Interview(AuditStamp):
    SCHEDULED = 'SE'
    ACTIVE = 'AC'
    COMPLETED = 'CT'

    INTERVIEW_STATUS = [
        (SCHEDULED,'Scheduled'),
        (ACTIVE,'Active'),
        (COMPLETED, 'Completed')
    ]

    id = models.UUIDField(primary_key=True, help_text="Interview Id of the interview object", editable=False, default=uuid.uuid4)
    title = models.CharField(max_length=100, help_text="title of interview")
    description = models.CharField(max_length=200, null=True, help_text="description about interview")
    role = models.CharField(max_length=50, help_text="role for interview")
    start_time = models.DateTimeField(auto_now_add=True, help_text='The start time of the interview')
    end_time = models.DateTimeField(default=None, null=True, help_text='The Completion time of the interview')
    status = models.CharField(choices=INTERVIEW_STATUS, max_length=50)

class Participant(AuditStamp):
    id = models.UUIDField(primary_key=True, help_text="Participant Id of the participant object", editable=False, default=uuid.uuid4)
    role = models.CharField(max_length=50, help_text="role of the participant")
    name = models.CharField(max_length=50, help_text="name of participant")
    email = models.EmailField()
    interview = models.ForeignKey('Interview', on_delete=models.CASCADE, null=False)
    end_time = models.DateTimeField(default=None, null=True, help_text='The Completion time of the interview')
    feedback = models.CharField(max_length=50, null=True)

class CodeSubmission(AuditStamp):
    QUEUED = 'QE'
    EXECUTING = 'EX'
    COMPLETED = 'CT'

    INTERVIEW_STATUS = [
        (QUEUED,'Queued'),
        (EXECUTING,'Executing'),
        (COMPLETED, 'Completed')
    ]

    id = models.UUIDField(primary_key=True, help_text="Id of the code submission object", editable=False, default=uuid.uuid4)
    interview = models.ForeignKey('Interview', on_delete=models.CASCADE, null=False)
    status = models.CharField(choices=INTERVIEW_STATUS, max_length=50)
    fire_time = models.DateTimeField(auto_now_add=True, help_text='when code execution was fired')





