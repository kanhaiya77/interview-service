from rest_framework import serializers
from django.db import transaction
from django.core.exceptions import ValidationError

from ws.models import Interview
from ws.models import Participant
from ws.models import CodeSubmission

from concurrent.futures import ThreadPoolExecutor
from cs_utils import requests
from django.conf import settings


class InterviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Interview
        fields = ('id', 'title', 'description', 'role', 'start_time', 'end_time', 'status', 'creator_id', 'last_modifier_id')
        read_only_fields=('id',)
        extra_kwargs = {'creator_id': {'write_only': True},
                'last_modifier_id': {'write_only': True}}

