from django.contrib import admin
from django.urls import path
from ws.views.interview_view import InterviewViewSet


urlpatterns = [

    #Interview
    path('/api/interviews/', InterviewViewSet.as_view({
        'post':'create_Interview',
        'get':'get_all_interviews'
    })),
    path('/api/interview/<str:interview_id>/', InterviewViewSet.as_view({
        'delete':'delete_Interview',
        'get':'get_interview'
    })),
    path(' /api/interview/<str:interview_id>/update/',InterviewViewSet.as_view({
        'patch':'update_interview'
    }))
]
